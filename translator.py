def translator():

    answer = 0

    try:
        answer = int(input("Что бы перевести сантиметры в дюймы введи 1, что бы перевести дюймы в сантиметры веди 2"
                           ", что бы выйти из программы введи 0: "))

    except ValueError:
        print('Введено не корректное значение')
        translator()

    if answer is 1:

        value = float(input("Введи значение для перевода: "))
        print('{} сантиметров = {} дюймов'.format(value, float(value / 2.54)))

        next_step = 0
        try:
            next_step = int(input("Для продолжения введи 1, для выхода нажми любую кнопку: "))
        except ValueError:
            exit()

        if next_step is 1:
            translator()

        else:
            exit()

    if answer is 2:
        value = float(input("Введи значение для перевода: "))
        print('{} дюймов = {} сантиметров'.format(value, value * 2.54))

        next_step = 0
        try:
            next_step = int(input("Для продолжения введи 1, для выхода нажми любую кнопку: "))
        except ValueError:
            exit()

        if next_step is 1:
            translator()

        else:
            exit()

    if answer is 0:
        exit()

    else:
        print('Введено не корректное значение')
        translator()


translator()
